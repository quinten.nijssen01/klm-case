# KLM Case

Within this project you can find the implementation of the case you gave me.
This project consists of 4 applications:

- `klm-travel-api` - the provided API with travel information;
- `back-end` - connects to the API and to the other two front-end applications;
- `front-end` - the main front-end this represents the fares and you can select origin and destination;
- `metrics-dashboard` - the dashboards which visualizes the metrics.

## Configuration

Within every project you can find a configuration file, here you can configure the ports, endpoints etc.
So when you want to change the port of one of the applications, make sure to change them everywhere where that
application is used. For example, when you change `back-end` port make sure that the `front-end` also talks to
that port.

## Start-up

Starting the `back-end` is the same as the API:

`./gradlew bootRun`

The front-ends are made in `React` and so you can start them using node.
Starting `front-end` & `metrics-dashboard`:

`npm start`

NOTE: Please first start the API before starting the back-end. There is a small bug within the back-end which let it
crash if it can't find the API.

NOTE: In order to get the most out of the experience you need to startup either the `klm-travel-api` & `back-end`,
before starting the `front-end`.
