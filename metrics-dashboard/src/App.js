import React, { Component } from 'react';
import './App.css';
import { fetchMetrics } from './server/travel';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { metrics: null };
  }

  componentDidMount() {
    fetchMetrics().then(metrics => this.setState({ metrics }));
  }

  render() {
    const { metrics } = this.state;

    if (!metrics) return null;

    const timeCard = {
      center: {
        value: metrics.avgTime,
        label: 'Average response time'
      },
      left: {
        value: metrics.maxTime,
        label: 'Maximum response time '
      },
      right: {
        value: metrics.minTime,
        label: 'Minimum response time'
      }
    };

    const responseCard = {
      center: {
        value: metrics.totalOK,
        label: 'Total OK responses'
      },
      left: {
        value: metrics.total4xx,
        label: 'Total 4xx responses'
      },
      right: {
        value: metrics.total5xx,
        label: 'Total 5xx responses'
      }
    };

    const totalCard = {
      center: {
        value: metrics.totalRequests,
        label: 'Total number of requests'
      }
    };

    return (
      <div className={'container'}>
        {this.renderCard("Time", timeCard)}
        {this.renderCard("Response", responseCard)}
        {this.renderCard("Total", totalCard)}
      </div>
    );
  }

  renderCard = (title, { center, left, right }) => (
    <div className={'cardContainer'}>
      <span className={'title'}>{title.toUpperCase()}</span>
      <div className={'mainValue'}>{this.renderValue(center)}</div>
      {left && right && (
        <div className={'footerContainer'}>
          {this.renderValue(left)}
          {this.renderValue(right)}
        </div>
      )}
    </div>
  );

  renderValue = ({ value, label }) => (
    <div className={'valueContainer'}>
      <b className={'value'}>{value}</b>
      <span className={'label'}>{label}</span>
    </div>
  );
}

export default App;
