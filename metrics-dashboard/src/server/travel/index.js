import { request } from '../helper';

export const fetchMetrics = async () => {
  const response = await request('GET', process.env.REACT_APP_GET_METRICS_ENDPOINT);
  return response.json();
};
