import { styled } from 'baseui';

export const Container = styled('div', {
  marginTop: '16px',
  display: 'flex',
  flex: '1',
  justifyContent: 'center'
});

export const CardContainer = styled('div', {
  width: '60%'
});

export const EndEnhancerContainer = styled('div', {});

export const TargetContainer = styled('div', {
  display: 'flex',
  justifyContent: 'space-between'
});

export const MarginRight = styled('div', {
  marginRight: '16px'
});
