import React, { Component } from 'react';

import { Card } from 'baseui/card';
import { ListItem, ListItemLabel } from 'baseui/list';

import * as styles from './styles';
import { DOLLAR_SIGN, EURO_SIGN, ORIGIN, DESTINATION } from '../../assets/text';

const EURO_CURRENCY = 'EUR';

/**
 * This class will represent all the fares for a specific origin and destination.
 * It shows in a list the price and the origin / destination details.
 *
 * Props:
 * - {@property fares} - The list of fares to show.
 */
export default class FareList extends Component {
  constructor(props) {
    super(props);

    this.state = { fares: null };
  }

  /**
   * When new fares are received from the props, update them in the component state.
   */
  static getDerivedStateFromProps(props, state) {
    if (props.fares !== state.fares) return { fares: props.fares };
    return null;
  }

  render() {
    const { fares } = this.state;

    return (
      <styles.Container>
        {fares && (
          <styles.CardContainer>
            <Card>
              <ol>{this.renderListItem(fares)}</ol>
            </Card>
          </styles.CardContainer>
        )}
      </styles.Container>
    );
  }

  /**
   * This function renders the fare.
   *
   * @param {object} fare - fare details.
   * @return {JSX}
   */
  renderListItem = fare => (
    <ListItem
      endEnhancer={() => (
        <styles.EndEnhancerContainer>
          <styles.TargetContainer>
            <span>{ORIGIN}</span>
            <styles.MarginRight />
            <ListItemLabel>{fare.origin.name}</ListItemLabel>
            <styles.MarginRight />
            <span>{fare.origin.code}</span>
          </styles.TargetContainer>
          <styles.MarginRight />

          <styles.TargetContainer>
            <span>{DESTINATION}</span>
            <styles.MarginRight />
            <ListItemLabel>{fare.destination.name}</ListItemLabel>
            <styles.MarginRight />
            <span>{fare.destination.code}</span>
          </styles.TargetContainer>
        </styles.EndEnhancerContainer>
      )}>
      <ListItemLabel>{this.getPrice(fare.currency, fare.amount)}</ListItemLabel>
    </ListItem>
  );

  /**
   * This function renders the price, when the currency is a euro show the euro sign.
   * Otherwise show the dollar sign.
   *
   * @param {String} currency - The currency.
   * @param {Number} amount   - The amount.
   * @return {string}
   */
  getPrice = (currency, amount) => (currency === EURO_CURRENCY ? `${EURO_SIGN}${amount}` : `${DOLLAR_SIGN}${amount}`);
}
