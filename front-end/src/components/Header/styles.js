import { styled } from 'baseui';

export const ImageContainer = styled('div', {
  display: 'flex',
  flex: '1',
  justifyContent: 'center',
  alignItems: 'center'
});