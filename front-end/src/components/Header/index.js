import React from 'react';

import * as styles from './styles';

import LOGO from '../../assets/images/klm-logo-100.png';

/**
 * This component is used to show the header of the page.
 * It shows a logo.
 */
export default function Header() {
  return (
    <styles.ImageContainer>
      <img src={LOGO} alt={'logo'} width={230} height={230} />
    </styles.ImageContainer>
  );
}
