import { styled } from 'baseui';

export const SelectorsContainer = styled('div', {
  display: 'flex',
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
});

export const MarginRight = styled('div', {
  marginRight: '16px'
});

export const SelectorContainer = styled('div', {
  display: 'flex',
  flex: 1,
  width: '100%',
  flexDirection: 'column',
  alignItems: 'flex-start',
  marginBottom: '16px'
});

export const optionsStyle = { height: '200px', position: 'absolute', marginTop: '60px' };
