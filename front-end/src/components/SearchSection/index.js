import React, { Component } from 'react';

import { Card } from 'baseui/card';
import { Input } from 'baseui/input';
import { StatefulMenu } from 'baseui/menu';
import { Button, SIZE } from 'baseui/button';
import { FormControl } from 'baseui/form-control';
import { ListItemLabel, MenuAdapter } from 'baseui/list';

import { fetchAirports, fetchFares } from '../../server/travel';

import * as text from '../../assets/text';
import * as styles from './styles';

let originTimeoutHandle;
let destinationTimeoutHandle;

const TYPE_REQUEST = 500; // The amount of time to wait until another airports request can be made.

/**
 * This component will show two input fields and a search button. It is used to determine the origin airport and
 * destination airport. While typing, new airports pop up for that specific term.
 *
 * Props:
 * - {@property onReceiveFares} function - Callback, which will be called when new fares arrived, it passes the new fares.
 */
export default class SearchSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pending: false,
      origin: { value: '', code: null, options: [] },
      destination: { value: '', code: null, options: [] }
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { origin, destination } = this.state;

    // If the origin airport changes, do a request to obtain new airports with that term.
    if (prevState.origin.value !== origin.value) {
      if (origin.value) this.airportChanged(true);
    }

    // If the destination airport changes, do a request to obtain new airports with that term.
    if (prevState.destination.value !== destination.value) {
      if (destination.value) this.airportChanged(false);
    }
  }

  /**
   * This function will be executed when a new value is typed by the user in the input field.
   * It handles the timeouts, those timeouts are implemented to not spam the {@see getAirports} call, since this is a
   * http request, this will slow down the application. It clears the timer when there already was a timer.
   *
   * @param {boolean} isOrigin - Indicates if its the origin airport.
   */
  airportChanged = isOrigin => {
    if (isOrigin && originTimeoutHandle) clearTimeout(originTimeoutHandle);
    if (!isOrigin && destinationTimeoutHandle) clearTimeout(destinationTimeoutHandle);

    const callback = this.getAirports.bind(this, isOrigin);

    if (isOrigin) originTimeoutHandle = setTimeout(callback, TYPE_REQUEST);
    else destinationTimeoutHandle = setTimeout(callback, TYPE_REQUEST);
  };

  render() {
    const { origin, destination, pending } = this.state;

    return (
      <Card>
        <styles.SelectorsContainer>
          <styles.SelectorContainer>
            <FormControl caption={text.ORIGIN_INPUT_CAPTION}>
              <Input
                value={origin.value}
                size={SIZE.large}
                placeholder={text.ORIGIN_INPUT_PLACEHOLDER}
                onChange={this.onOriginChange}
              />
            </FormControl>
            {this.showOptions(origin) ? this.renderOptions(origin.options, true) : null}
          </styles.SelectorContainer>
          <styles.MarginRight />
          <styles.SelectorContainer>
            <FormControl caption={text.DESTINATION_INPUT_CAPTION}>
              <Input
                size={SIZE.large}
                value={destination.value}
                onChange={this.onDestinationChange}
                placeholder={text.DESTINATION_INPUT_PLACEHOLDER}
              />
            </FormControl>
            {this.showOptions(destination) ? this.renderOptions(destination.options, false) : null}
          </styles.SelectorContainer>
        </styles.SelectorsContainer>
        <Button disabled={this.isSearchDisabled()} isLoading={pending} onClick={this.onSearchClick}>
          {text.SEARCH}
        </Button>
      </Card>
    );
  }

  /**
   * This function renders the options box. It shows all possible options which are typed in the input field.
   * It overrides the current options for a custom representation of the options.
   * Every option is rendered by {@see renderMenuItem}.
   *
   * @param {object}  items    - The options to render.
   * @param {boolean} isOrigin - Indicates if it should render the destination box or origin box.
   * @return {JSX}
   */
  renderOptions = (items, isOrigin) => (
    <StatefulMenu
      items={items}
      onItemSelect={isOrigin ? this.onOriginItemSelect : this.onDestinationItemSelect}
      overrides={{
        List: { style: styles.optionsStyle },
        Option: { props: { overrides: { ListItem: { component: React.forwardRef(this.renderMenuItem) } } } }
      }}
    />
  );

  renderMenuItem = (props, ref) => (
    <MenuAdapter {...props} ref={ref}>
      <ListItemLabel description={props.item.description}>{`${props.item.name} - ${props.item.code}`}</ListItemLabel>
    </MenuAdapter>
  );

  /**
   * This function will fetch a list of airports which is requested. Those airports will be shown in the
   * options box.
   *
   * @param {boolean} isOrigin - Indicates if its the origin airport.
   */
  getAirports = isOrigin => {
    const airport = isOrigin ? 'origin' : 'destination';

    fetchAirports(this.state[airport].value).then(options =>
      this.setState({ [airport]: { ...this.state[airport], options: options instanceof Array ? options : [] } })
    );
  };

  /**
   * This function determines if the options box should be rendered. It doesn't render when there is no value
   * typed yet, or when there is selected an item.
   *
   * @param  {object} state - The airport state.
   * @return {boolean}
   */
  showOptions = state => !(state.value.length === 0 || state.itemSelected);

  isSearchDisabled = () => this.state.origin.code === null || this.state.destination.code === null;

  /**
   * This function is executed when a search action is performed, it retrieves the fares.
   * It first says that it's pending fetching the fares.
   * Then it checks if the fields are filled in, if soe it does a request to the server to obtain the fares.
   * It passes the resulted fares back the {@see Main} who will pass it to the {@see FareList} component.
   */
  onSearchClick = () => {
    this.setState({ ...this.state, pending: true });

    const { code: originCode } = this.state.origin;
    const { code: destinationCode } = this.state.destination;

    if (typeof originCode === 'string' && typeof destinationCode === 'string') {
      fetchFares(originCode, destinationCode).then(fares => {
        this.props.onReceiveFares(fares);
        this.setState({ ...this.state, pending: false });
      });
    }
  };

  /*
   * Some handler methods to adjust the state, when the input changes.
   */

  onOriginItemSelect = ({ item }) =>
    this.setState({ origin: { ...this.state.origin, value: item.description, code: item.code, itemSelected: true } });

  onDestinationItemSelect = ({ item }) =>
    this.setState({
      destination: { ...this.state.destination, value: item.description, code: item.code, itemSelected: true }
    });

  onOriginChange = event =>
    this.setState({ origin: { ...this.state.origin, value: event.target.value, itemSelected: false } });

  onDestinationChange = event =>
    this.setState({ destination: { ...this.state.destination, value: event.target.value, itemSelected: false } });
}
