import { config } from 'dotenv';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import './index.css';

import { Provider as StyletronProvider } from 'styletron-react';
import { BaseProvider, LightTheme } from 'baseui';
import { Client as Styletron } from 'styletron-engine-atomic';

import Main from './screens/main';

config();

const engine = new Styletron();

class App extends Component {
  render() {
    return (
      <StyletronProvider value={engine}>
        <BaseProvider theme={LightTheme}>
          <Main />
        </BaseProvider>
      </StyletronProvider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
