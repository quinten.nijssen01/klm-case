import { request } from '../helper';

export const fetchAirports = async searchTerm => {
  const response = await request('GET', process.env.REACT_APP_GET_AIRPORTS_ENDPOINT, `term=${searchTerm}`);
  return response.json();
};

export const fetchFares = async (origin, destination) => {
  const response = await request('GET', `${process.env.REACT_APP_GET_FARES_ENDPOINT}/${origin}/${destination}`);
  return response.json();
};
