const MIME_JSON = 'application/json';

export const DEFAULT_HEADERS = {
  Accept: MIME_JSON,
  'Content-Type': MIME_JSON
};

export const request = async (method, endpoint, query) => {
  const urlQuery = query ? `?${query}` : '';

  try {
    return fetch(`${process.env.REACT_APP_BACK_END_BASE_URL}${endpoint}${urlQuery}`, {
      method,
      headers: {
        ...DEFAULT_HEADERS
      }
    });
  } catch (exception) {
    // TODO: HANDLE ERROR.
  }
};
