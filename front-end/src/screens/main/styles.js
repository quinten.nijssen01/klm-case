import { styled } from 'baseui';

import { KLM_BASE_COLOR } from '../../assets/styles/colors';

export const BackgroundBanner = styled('div', {
  width: '100%',
  height: '100%',
  boxSizing: 'border-box',
  padding: '0 40px 16px 40px',
  backgroundColor: KLM_BASE_COLOR,
  boxShadow: '-6px 0px 15px 2px rgba(0,0,0,0.22)'
});
