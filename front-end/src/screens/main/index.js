import React, { Component } from 'react';

import { createTheme, lightThemePrimitives, ThemeProvider } from 'baseui';

import Header from '../../components/Header';
import FareList from '../../components/FareList';
import SearchSection from '../../components/SearchSection';

import * as styles from './styles';
import { KLM_BASE_COLOR, KLM_BASE_HOVER_COLOR } from '../../assets/styles/colors';

/**
 * This is the main (only) screen of the application. It loads all the defined components which will full-fill the
 * assignment.
 *
 * I use the library Base Web, this is to make the component a little bit nice then if I would do it myself with css.
 * This library let you use 'StyledComponents' which are components that can be rendered as JSX and has a certain style.
 * This is not the best way of doing your styles, but the library did'nt allow the use of CSS.
 */
export default class Main extends Component {
  constructor(props) {
    super(props);

    this.state = { fares: null };
  }

  theme = {
    colors: {
      buttonPrimaryFill: KLM_BASE_COLOR,
      buttonPrimaryHover: KLM_BASE_HOVER_COLOR,
      buttonPrimaryActive: KLM_BASE_COLOR,
      buttonSecondaryText: KLM_BASE_COLOR,
      buttonTertiaryText: KLM_BASE_COLOR,
      buttonTertiarySelectedText: KLM_BASE_COLOR,
      buttonMinimalText: KLM_BASE_COLOR
    }
  };

  render() {
    const { fares } = this.state;

    return (
      <ThemeProvider theme={createTheme(lightThemePrimitives, this.theme)}>
        <styles.BackgroundBanner>
          <Header />
          <SearchSection onReceiveFares={this.onReceiveFares} />
        </styles.BackgroundBanner>
        <FareList fares={fares} />
      </ThemeProvider>
    );
  }

  onReceiveFares = fares => this.setState({ fares });
}
