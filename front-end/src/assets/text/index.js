export const EURO_SIGN = '€';
export const DOLLAR_SIGN = '$';

export const SEARCH = 'Search';

export const ORIGIN = 'Origin';
export const ORIGIN_INPUT_PLACEHOLDER = 'Origin ...';
export const ORIGIN_INPUT_CAPTION = 'Select your origin';

export const DESTINATION = 'Destination';
export const DESTINATION_INPUT_PLACEHOLDER = 'Destination ...';
export const DESTINATION_INPUT_CAPTION = 'Select your destination';
