package com.afkl.cases.df.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;

import java.util.concurrent.Executor;

/**
 * Some configuration per recourse, it permits everyone to call this application and enables cors. It creates
 * an oauth template. It initializes an async thread pool executor for async calls.
 */
@Configuration
@EnableAsync
@EnableWebSecurity
class ResourceConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * This function will make the back-end service open for everyone. So no authentication is required.
     * It basically disables the default spring boot security. It also enables cors.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().authorizeRequests().antMatchers("/").permitAll();
    }

    /**
     * This function creates an Rest Template with an oauth 2 wrapper. It makes sure that every http request
     * that is made with this template is authenticated.
     */
    @Bean
    public OAuth2RestTemplate oauth2RestTemplate(OAuth2ProtectedResourceDetails details) {
        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(details);

        /* To validate if required configurations are in place during startup */
        oAuth2RestTemplate.getAccessToken();

        return oAuth2RestTemplate;
    }

    /**
     * This function returns and async executor, which is used for async calls.
     */
    @Bean
    public Executor asyncExecutor() {
        return new ThreadPoolTaskExecutor();
    }

}
