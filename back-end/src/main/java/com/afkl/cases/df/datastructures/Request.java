package com.afkl.cases.df.datastructures;

public class Request {

    private String method;
    private String endpoint;
    private Status status;

    private long time;

    public Request(String method, String endpoint, Status status, long time) {
        this.method = method;
        this.endpoint = endpoint;
        this.status = status;
        this.time = time;
    }

    public String getMethod() {
        return method;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getTime() {
        return time;
    }
}
