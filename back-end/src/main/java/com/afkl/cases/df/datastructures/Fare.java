package com.afkl.cases.df.datastructures;

public class Fare {

    private Double amount;
    private String currency;
    private Airport origin;
    private Airport destination;

    public Fare(Double amount, String currency, Airport origin, Airport destination) {
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Airport getOrigin() {
        return origin;
    }

    public Airport getDestination() {
        return destination;
    }

}
