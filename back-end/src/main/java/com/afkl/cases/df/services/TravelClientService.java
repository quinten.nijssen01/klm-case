package com.afkl.cases.df.services;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

/**
 * This service will setup calls to the Travel API, and returns the response of the invoked call.
 * It is the client class of the Travel API.
 */
@Service
public class TravelClientService {

    @Autowired
    private RequestService requestService;

    public JSONObject getAirports(String term) {
        return requestService.request("/airports", "term=" + term);
    }

    @Async
    public CompletableFuture<JSONObject> getFare(String origin, String destination) {
        JSONObject response = requestService.request("/fares/" + origin + "/" + destination, "");
        return CompletableFuture.completedFuture(response);
    }

    @Async
    public CompletableFuture<JSONObject> getAirport(String code) {
        JSONObject response = requestService.request("/airports/" + code, "");
        return CompletableFuture.completedFuture(response);
    }

}
