package com.afkl.cases.df.datastructures;

public class Metric {

    private int totalRequests;
    private int totalOK;
    private int total4xx;
    private int total5xx;

    private long avgTime;
    private long minTime;
    private long maxTime;

    public Metric(int totalRequests, int totalOK, int total4xx, int total5xx, long avgTime, long minTime, long maxTime) {
        this.totalRequests = totalRequests;
        this.totalOK = totalOK;
        this.total4xx = total4xx;
        this.total5xx = total5xx;
        this.avgTime = avgTime;
        this.minTime = minTime;
        this.maxTime = maxTime;
    }

    public int getTotalRequests() {
        return totalRequests;
    }

    public int getTotalOK() {
        return totalOK;
    }

    public int getTotal4xx() {
        return total4xx;
    }

    public int getTotal5xx() {
        return total5xx;
    }

    public long getAvgTime() {
        return avgTime;
    }

    public long getMinTime() {
        return minTime;
    }

    public long getMaxTime() {
        return maxTime;
    }
}
