package com.afkl.cases.df.services;

import com.afkl.cases.df.datastructures.Request;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * This class is a service to help calculate the metrics, like averages, sums etc.
 */
@Service
public class MetricService {

    private ArrayList<Request> metrics;

    private int totalOK;
    private int total4xx;
    private int total5xx;

    public MetricService(ArrayList<Request> metrics) {
        this.metrics = metrics;

        this.totalOK = 0;
        this.total4xx = 0;
        this.total5xx = 0;

        countStatus(metrics);
    }

    private void countStatus(ArrayList<Request> metrics){
        for (Request metric : metrics) {
            switch (metric.getStatus()) {
                case S_OK:
                    this.totalOK++;
                    break;
                case S_4XX:
                    this.total4xx++;
                    break;
                case S_5XX:
                    this.total5xx++;
                    break;
            }
        }
    }

    public int getTotalOK() {
        return totalOK;
    }

    public int getTotal4xx() {
        return total4xx;
    }

    public int getTotal5xx() {
        return total5xx;
    }

    public long getAvgTime() {
        long total = 0;
        for (Request metric : metrics) total += metric.getTime();
        return total / metrics.size();
    }

    public long getMaxTime() {
        long max = 0;
        for (Request metric : metrics) {
            if (metric.getTime() > max) max = metric.getTime();
        }
        return max;
    }

    public long getMinTime() {
        long min = metrics.get(0).getTime();
        for (Request metric : metrics) {
            if (metric.getTime() < min) min = metric.getTime();
        }
        return min;
    }
}
