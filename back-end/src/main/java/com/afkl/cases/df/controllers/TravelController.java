package com.afkl.cases.df.controllers;

import com.afkl.cases.df.datastructures.Airport;
import com.afkl.cases.df.datastructures.Fare;
import com.afkl.cases.df.datastructures.Metric;
import com.afkl.cases.df.datastructures.Request;
import com.afkl.cases.df.services.IOService;
import com.afkl.cases.df.services.MetricService;
import com.afkl.cases.df.services.TravelClientService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * The main controller, within this class all the endpoints of this application are defined.
 */
@RestController
public class TravelController {

    @Autowired
    private TravelClientService travelClientService;
    @Autowired
    private IOService ioService;

    @RequestMapping(value = "${get-airports}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAirports(@RequestParam String term) {
        try {
            // Fetch airport locations.
            JSONArray locations = travelClientService.getAirports(term).getJSONObject("_embedded").getJSONArray("locations");
            // Remove all the parent properties from the response.
            IntStream.range(0, locations.length()).forEach(i -> locations.getJSONObject(i).remove("parent"));
            return locations.toString();
        } catch (Exception e) {
            return "[]";
        }

    }

    @RequestMapping(value = "${get-fares}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getFares(@PathVariable String origin, @PathVariable String destination) {
        try {
            CompletableFuture<JSONObject> fareResponse = travelClientService.getFare(origin, destination);
            CompletableFuture<JSONObject> originResponse = travelClientService.getAirport(origin);
            CompletableFuture<JSONObject> destinationResponse = travelClientService.getAirport(destination);

            Airport originAirport = new Airport(originResponse.get().getString("name"), origin, originResponse.get().getString("description"));
            Airport destinationAirport = new Airport(destinationResponse.get().getString("name"), destination, destinationResponse.get().getString("description"));

            Fare fare = new Fare(fareResponse.get().getDouble("amount"), fareResponse.get().getString("currency"), originAirport, destinationAirport);

            return new JSONObject(fare).toString();
        } catch (Exception e) {
            return "{}";
        }
    }

    @RequestMapping(value = "${get-metrics}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getMetrics() {
        try {
            ArrayList<Request> metrics = ioService.get();
            MetricService metricService = new MetricService(metrics);

            Metric metric = new Metric(metrics.size(),
                    metricService.getTotalOK(),
                    metricService.getTotal4xx(),
                    metricService.getTotal5xx(),
                    metricService.getAvgTime(),
                    metricService.getMinTime(),
                    metricService.getMaxTime());

            return new JSONObject(metric).toString();
        } catch (Exception e) {
            return "{}";
        }
    }
}
