package com.afkl.cases.df.datastructures;

public enum Status {
    S_OK, S_5XX, S_4XX, S_NAS
}
