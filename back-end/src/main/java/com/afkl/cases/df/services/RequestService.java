package com.afkl.cases.df.services;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * This class does a request, with an oauth template, which will ensure that the call sends a token.
 */
@Service
public class RequestService {
    @Value("${travel-api-url}")
    private String travelApiUrl;

    @Autowired
    private OAuth2RestTemplate oAuth2RestTemplate;

    public JSONObject request(String endpoint, String query) {
        return new JSONObject(Objects.requireNonNull(oAuth2RestTemplate.getForObject(travelApiUrl + endpoint + "?" + query, String.class)));
    }
}
