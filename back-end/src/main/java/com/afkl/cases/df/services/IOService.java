package com.afkl.cases.df.services;

import com.afkl.cases.df.datastructures.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;

@Service
public class IOService {

    @Value("${metrics-db-path}")
    private String metricsDbPath;

    /**
     * This function appends a new request to the stored file.
     */
    void append(Request request) {
        ArrayList<Request> requestdb = read();
        requestdb.add(request);
        write(requestdb);
    }

    /**
     * This function retrieves all the stored requests.
     */
    public ArrayList<Request> get() {
        return read();
    }

    /**
     * Write to the db file.
     */
    private void write(ArrayList<Request> requests) {
        try {
            Writer writer = new FileWriter(metricsDbPath);
            Gson gson = new GsonBuilder().create();

            gson.toJson(requests, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read from the db file.
     */
    private ArrayList<Request> read() {
        try {
            Gson gson = new Gson();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(metricsDbPath));

            return gson.fromJson(bufferedReader, new TypeToken<ArrayList<Request>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            return new ArrayList<Request>();
        }
    }
}