package com.afkl.cases.df.services;

import com.afkl.cases.df.datastructures.Request;
import com.afkl.cases.df.datastructures.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This is the middleware class which will monitor and store some information about the request & response.
 */
@Component
@Order(1)
public class MonitorService implements Filter {

    @Autowired
    IOService ioService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        long time = System.currentTimeMillis();

        try {
            chain.doFilter(request, response);
        } finally {
            time = System.currentTimeMillis() - time;

            Request newRequest = new Request(req.getMethod(),
                    getEndpoint(req.getRequestURI(), req.getQueryString()),
                    determineStatus(res.getStatus()),
                    time);

            ioService.append(newRequest);
        }
    }

    private String getEndpoint(String uri, String query) {
        if (query == null) return uri;
        return uri + "?" + query;
    }

    /**
     * This function places the status within a section of status codes.
     */
    private Status determineStatus(int status) {
        if (status == 200) return Status.S_OK;
        else if (status >= 400 && status < 500) return Status.S_4XX;
        else if (status >= 500 && status < 600) return Status.S_5XX;
        return Status.S_NAS;
    }

}